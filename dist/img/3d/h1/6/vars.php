<?php
// основные константы, переменные и настройки
// ******************************************

// база данных
// ----------------------------------
define('DB_NAME', 'hitigra');
define('DB_USER', 'hitigra');
define('DB_PASSWORD', 'ash45ags');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8');
define('DB_PREFIX', 'hit_');

// основные папки и файлы
// ----------------------------------
define('F_IN', F_IMEX.'inbox/');			// директория для импорта данных на сайт
define('F_OUT', F_IMEX.'outbox/');			// директория для экспорта данных в 1С
define('F_CAT_KIND', 'kind');				// справочник - виды
define('F_CAT_TYPES', 'type');				// справочник - типы
define('F_CAT_GIFT', 'gift');				// справочник - подарки
define('F_CAT_CATEGORY', 'cat');			// справочник - категории
define('F_CAT_RELATED', 'goods');			// справочник - сопутствующие товары
define('F_CAT_BOOK', 'book');				// справочник!
define('F_CAT_GOODS', 'goods');				// каталог товаров
define('F_CAT_IMPORT', 'import');			// внутренний лог - выгрузки
define('F_CAT_ORDERS', 'order');			// внутренний справочник - заказы
define('F_CAT_ORDER_ITEMS', 'order_items');	// внутренний справочник - позиции заказа
define('F_EXT', '.csv');					// расширение входных файлов
define('F_DB_PREFIX', 'cat_');				// префикс в базе данных
define('F_SEP', ';');						// разделитель полей в файле
define('F_SEP_META', ',');					// разделитель полей в колонки со списками/справочниками
define('F_SKIP_FIRST_LINE', false);			// не обрабатывать первую строку в файле

define('F_CAT_INORDER_REGEXP','|order\d+\.csv|i');

// формат данных
// ----------------------------------
global $fd,$f2a;

// номер колонки в CSV = название поля в бд
$fd = array();
$fd[F_CAT_GOODS] = array(
 0  => 'id',
 3  => 'kid',
 1  => 'name',
 2  => 'name_en',
 7  => 'gamer_min',
 8  => 'gamer_max',
 9  => 'long',
 10 => 'age',
 11 => 'dizz',
 12 => 'price',
 13 => 'price_old',
 14 => 'box_height',
 15 => 'box_length',
 16 => 'box_width',
 17 => 'box_weight',
 18 => 'remain_shop',
 19 => 'remain_stock',
 20 => 'rating',//20
 22 => 'preorder',//22
);
$fd[F_CAT_ORDERS] = array(
 0  => 'id',
// 1  => 'date',
// 2  => 'date',
 4  => 'name',
 5  => 'email',
 6  => 'phone',
 11 => 'delivery',
 13 => 'delivery_price',
 12 => 'addr',
 7  => 'personal_discount',
 8  => 'individual_discount',
 9  => 'student_discount',
 10 => 'birthday_discount',
 14 => 'total',
 3  => 'state',
);

// справочник = номер колонки в CSV 
$s2a = array(
 'items'  => 15,
 'counts' => 16,
 'prices' => 17,
);

// справочник = номер колонки в CSV 
$f2a = array(
 F_CAT_TYPES => 4,
 F_CAT_CATEGORY => 5,
 F_CAT_GIFT => 6,
 F_CAT_RELATED => 21,//21
);


?>