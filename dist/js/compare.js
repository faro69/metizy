$(document).ready( function(){

var distinct_arr = []; // массив различающихся параметров
var still_removing = false; // не давать сворачивать/разворачивать одинаковые параметры пока происходит анимация удаления ряда  

  $('.compare_products_list').each(function() {
    var widthList = 0;
	
  	$(this).find('li').each(function() {
  		widthList += $(this).outerWidth();
      });
  	
  	$(this).width(widthList);
  });

  var compareScrollApi = $('.compare_products_b').jScrollPane({
  	showArrows: false
  }).data('jsp');

  $('.compare_products_wrap .compare_btn').click(function(e) {
  	var compareList = $(this).siblings('.compare_products_b').find('.compare_products_list'),
  		currentLeft = compareScrollApi.getContentPositionX(),
  		widthLiFirst = compareList.find('li:first').outerWidth(),
  		widthLi = compareList.find('li:eq(1)').outerWidth(),
  		nextLi, padLi, destX;
  	
  	nextLi = Math.floor((currentLeft - widthLiFirst)/widthLi) + 1;
  	padLi = parseInt(compareList.find('li:eq(1)').css('padding-left'));
  		
  	if ($(this).hasClass('prev')) {
  		nextLi -= 1;
  		padLi = 0;
  	} else {
  		nextLi += 1;
  	}
  	
  	if (nextLi < 0) nextLi = 0;
  	
  	destX = compareList.find('li:eq(' + nextLi + ')').position().left + padLi;	
  	compareScrollApi.scrollToX(destX, true);
  	
      e.preventDefault();
  });


  /*$('.compare_products_list .btn_remove').click(function(e) {
  	$(this).closest('li').hide(function(){
  		var $list = $(this).closest('.compare_products_list'),
  			widthList = 0;
  			
  		$(this).remove();		
  		
  		$list.find('li').each(function() {
  			widthList += $(this).outerWidth();
  			console.log(widthList);
  		});
  		
  		$list.width(widthList);
  		compareScrollApi.reinitialise();
  	});
  	
      e.preventDefault();
  });*/

  function remove_from_compare(me) {
    var $list = me.closest('.compare_products_list'),
  		widthList = heightList =0;
  		
  	me.remove();
  	
  	$list.find('li').each(function() {
  		widthList += $(this).outerWidth();
  		//console.log(widthList);
  	});
  	
  	$list.width(widthList);
  	compareScrollApi.reinitialise();
    $('.jspContainer').animate({height: $('.jspPane').height()}, 200, 'easeOutQuad', function(){
      still_removing = false;
    });
  }
  // -----------------------------------------------------------------------------------------------------------
  
  // показываем кружочки
  var nbasket = ($('.b_icon_basket2 .num').length)?$('.b_icon_basket2 .num'):$('.b_icon_basket .num');
  if (nbasket.length) {
    var nbask = parseInt(nbasket.text());
    if (nbask > 0) nbasket.animate({opacity: 1}, 500);
  }
  
  var nbox = ($('.b_icon_view2 .num').length)?$('.b_icon_view2 .num'):$('.b_icon_view .num');
  if (nbox.length) {
    var ncoll = parseInt(nbox.text());
    if (ncoll > 0) nbox.animate({opacity: 1}, 500);
  }
  
  // добавить в сравнение
  $('.add_to_compare').live('click', function(e){
    
    var myspan = $(this).find('.add_a');
    var mya = $(this);
    var v = mya.attr('href').substr(1);
    var params = [];
    if (v) {
      params = v.split('-'); // id - cid
    }
    if (params.length > 1) {
      e.preventDefault();
      $.ajax({
        type: 'GET',
        url: location.href,
        dataType: 'json',
        data: {cwp_ajax: "compare", par: params},
        success: function(skolko) {
          if (skolko) {
            if($('.cmp_sourse').length) var hrf = $('.cmp_sourse').attr('href');
            else var hrf = '';
            mya.attr('href', hrf);
            myspan.html('В сравнении');
            if (nbox.length) {
              nbox.animate({opacity: 0}, 200, 'easeOutQuad', function(){
                nbox.html(skolko);
                nbox.animate({opacity: 1}, 200, 'easeOutQuad');
              });
            }
          }
        }
      });
    }
  });
  
  // удалить из сравнения
  $('.delete_compare').live('click', function(e){
    var mya = $(this);
    var tid = mya.attr('href').substr(1);
    if (tid) {
      tid = parseInt(tid);
      e.preventDefault();
      $.ajax({
        type: 'GET',
        url: location.href,
        dataType: 'json',
        data: {cwp_ajax: "dcompare", par: tid},
        success: function(skolko) {
          var myli = mya.closest('li');
          still_removing = true;
          mya.closest('li').animate({opacity: 0}, 200, 'easeOutQuad', function() {
            myli.animate({width: 0, padding: 0}, 1000, 'easeOutBounce', function() {
              remove_from_compare(myli);
              //myli.remove();
            });
          });
          if(!skolko) {
            $('.compare_param_b').animate({opacity: 0}, 200, 'linear', function() {
              $('.compare_param_b').remove();
            });
            $('.compare_products_wrap').animate({opacity: 0}, 200, 'linear', function() {
              $('.compare_products_wrap').remove();
              $('#content section').append('<p class="cmp_err">Нет товаров для сравнения.</p>');
            });
          }
          if (nbox.length) {
            nbox.animate({opacity: 0}, 200, 'easeOutQuad', function(){
              nbox.html(skolko);
              if (skolko) nbox.animate({opacity: 1}, 200, 'easeOutQuad');
            });
          }
        }
      });
    }
  });
  
  $('.all_params').live('click', function(e){
    e.preventDefault();
    if (!$(this).hasClass('pars_current') && !still_removing) {
      $('.compare_filters a').toggleClass('pars_current');
      
      $('.compare_products_list li, .compare_param_list li').each(function(li_counter) {
        var cmp_pr = ($(this).find('.compare_row').length)?$(this).find('.compare_row'):$(this);
        if (cmp_pr.length) {
          cmp_pr.each(function(prop_counter) {
            if ($.inArray(prop_counter, distinct_arr) == -1 ) { $(this).slideDown(400); }
          });
        }
      });
      
      $('.compare_param_list li').each(function(prop_counter) {
        if ($.inArray(prop_counter, distinct_arr) == -1 ) { $(this).slideDown(400); }
      });
      
    }
  });

  $('.distinct_params').live('click', function(e) {
    e.preventDefault();
    if (!$(this).hasClass('pars_current') && !still_removing) {
      $('.compare_filters a').toggleClass('pars_current');
      var cmp_arr = [];
      if ($('.compare_products_list li').length>1) {
        $('.compare_products_list li').each(function(li_counter) {
          var cmp_p = $(this).find('.compare_row');
          if (cmp_p.length) {
            cmp_p.each(function(prop_counter) {
              if( !$.isArray( cmp_arr[li_counter] ) ) cmp_arr[li_counter] = new Array();
              cmp_arr[li_counter][prop_counter] = $(this).text().toString();
            });
          }
        });
        
        if ( $.isArray(cmp_arr) ) {
        
          var pars_num = cmp_arr[0].length;
          var i = 0;
          distinct_arr = [];
          while (i < pars_num) {
            var old_par_val = '', first = true;
            for (var li in cmp_arr) {
              if (first == true ) { old_par_val = cmp_arr[li][i]; first = false }
              else {
                if (old_par_val != cmp_arr[li][i]) { distinct_arr.push(i); break; }
              }
            }
            i++;
          }
          
          $('.compare_products_list li').each(function(li_counter) {
            var cmp_pr = $(this).find('.compare_row');
            if (cmp_pr.length) {
              cmp_pr.each(function(prop_counter) {
                if ($.inArray(prop_counter, distinct_arr) == -1 ) { $(this).slideUp(400); }
              });
            }
          });
          
          $('.compare_param_list li').each(function(prop_counter) {
            if ($.inArray(prop_counter, distinct_arr) == -1 ) { $(this).slideUp(400); }
          });
          
        }
      }
    }
  });
  
});