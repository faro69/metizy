$(document).ready( function(){

//click out
'use strict';

    $.fn.clickOut = function (data, fn) {
        $(document).click(
            $.proxy(function (event) {
                if (!$(event.target).is(this) && this.has(event.target).length === 0) {
                    event.delegateTarget = this.get();

                    if (fn === undefined) {
                        data.call(this, event);
                    } else {
                        fn.call(this, event);
                    }
                }
            }, this)
        );
    };

//sliders
$('#index_slides').slides({
	preload: true,
	preloadImage: '/wp-content/themes/metizy/img/loading.gif',
	play: 3000,
	pause: 1000,
	pagination: true,
	effect:'fade',
	slideEasing: "easeOutBounce",
	slideSpeed:800,
	hoverPause: true
});


$('.step_slider').slides({
    preload: true,
    preloadImage: '/wp-content/themes/metizy/img/loading.gif',
 
    pause: 1000,
    pagination: true,
    slideSpeed:500,
    hoverPause: true
});

slideItemWidth();
function slideItemWidth(){
	var DocumentWidth=$(document).width();
	$('#index_slides .slides_control > div').width(DocumentWidth);
}


$(window).resize(function(){
	slideItemWidth();
});




$('.char-row .words a').live('click',function(){
return false;
});

//tooltips
$('.b_icon_tooltip, .char-row .words a').live('click',function(){
$('.b_icon_tooltip').removeClass('active');
$(this).addClass('active');
$('.tooltip_block').hide();
$(this).find('.tooltip_block').show();
$('.b_icon_tooltip, .char-row .words a').clickOut(function(){
$('.b_icon_tooltip, .char-row .words a').removeClass('active');
$('.tooltip_block').hide();
});

$('.tooltip_block .b_icon_close').live('click',function(){
$('.tooltip_block').hide();
$('.b_icon_tooltip, .char-row .words a').removeClass('active');
return false;
});

});



//tooltips height
var tooltipHeight=$('.tooltip_block.right').height();
$('.tooltip_block.right').each(function(){
	var tooltipHeight=$(this).height();
	$(this).css('top',-tooltipHeight/2+'px');
});

var tooltipHeight2=$('.tooltip_block.left').height();
$('.tooltip_block.left').each(function(){
    var tooltipHeight2=$(this).height();
    $(this).css('top',-tooltipHeight2/2+'px');
});

//Fixed top panel

var notPanels=$('body').not('#index, #er404');
notPanels.prepend('<div class="top_panel"><div class="container"></div></div>');
$('.brand').clone().appendTo('.top_panel .container');
$('.product_bars').clone().appendTo('.top_panel .container');
//$('.search-box').clone().appendTo('.top_panel .container');
$('.top_panel').append('<div class="animate_shadow"></div>');
$('.top_panel').after('<div class="shadow-cover"></div>');
function FixedTop() {
	if (($(window).scrollTop() > 104)) {
		$('.top_panel .animate_shadow').fadeIn(320);
		$('.top_panel').show();
		notPanels.find('header .panel').css('visibility','hidden');

	} else {
		
		$('.top_panel').hide().find('.animate_shadow').hide();
		notPanels.find('header .panel').css('visibility','visible');


	}
}



$(window).scroll(function(){

	FixedTop();
	
	
});
/*
// search drop 
$('.search-panel input[type="text"]').keyup(
	function(){
	   $('.search-panel .loader').show();
	   $('.search-drop').show();
	   $('.search-panel input[type="text"]').val($(this).val());
});



$('.search-panel input[type="text"], .search-panel .search-drop').clickOut(
	function(){
	    $('.search-drop').hide();
	    $('.search-panel .loader').hide();
});
*/

//radio & checkbox 
$('.radio-group .radio label').live('click',function(){
    $(this).parents('.radio-group').find('.radio label').removeClass('checked');
});

$('.checkbox label, .radio label').live('click',function(){
    if ($(this).find('input[type="checkbox"], input[type="radio"]').is(':checked')) { 
        $(this).addClass('checked'); } 

    else { 
        $(this).removeClass('checked'); } 
});


//select 

$('.select-holder').live('click',function(){ 
    $('.select-holder').not(this).removeClass('active');
    $('.select-holder').not(this).find('.select-drop').hide();
    
    $(this).toggleClass('active');
    $(this).find('.select-drop').toggle();
    $(this).clickOut(function(){
        $(this).removeClass('active');
        $(this).find('.select-drop').hide();
 });
});

 $('.select-drop li').live('click',function(event){
       $('.select-drop li').removeClass('active');
        $(this).addClass('active');
        $(this).parents('.select-drop').hide();
        $(this).parents('.select-holder').removeClass('active').find('.text-holder').text($(this).text()).addClass('selected');
        event.stopPropagation();
    });

    
 //order history
 $('.order-row .panel').click(function(){
    $(this).toggleClass('current').nextAll('.composition').slideToggle(300);
 });


// masonry products
$('#ajax-products').masonry({
	itemSelector: '.product'
});
$('#ajax-products').masonry( 'reload' );

    $(".char").pxgradient({ 
       step: 2,
       colors: ["#505050","#000"], 
       dir: "y" 
});



// scroll	
$('.faq .questions_list a, .site_help .questions li a').click(function() {
    var str = $(this).attr('href');
    $.scrollTo(str, 500, { margin:false, offset:-75});
    return false;
});




// scroll top
if($(document).find('.scroll_top').length){ 
  var scrollup = function(e){
     var wt = $(window).scrollTop();
     var st = $('.scroll_top');
     if(wt >= 10){
        if(!st.is(':visible') && !st.is(':animated')){
           st.fadeIn(300);
       }
   }else{
    if(st.is(':visible') && !st.is(':animated')){
       st.fadeOut(300);
   }
}}

$(window).bind('scroll',scrollup);
$('.scroll_top').bind('click',function(e){
 e.preventDefault();
 $('html:not(:animated),body:not(:animated)').stop().animate({scrollTop: 0}, 600);

});
}

$('.label').click(function(){
    $(this).toggleClass('checked');
});

/*
var discount=$('.account .discount').text()
$('#change-profile').toggle(
function(){
	$(this).text('Сохранить изменения');
	;
	$('.account .discount').text('?');
	$('.account .disabled').removeClass('disabled').removeAttr('readonly');

	
},
function(){
	$(this).text('редактировать данные');
	$('.account .discount').text(discount);
	$('.account dd .select, .account dd input').addClass('disabled').attr('readonly','readonly');

});
*/

//drops 
$('.status-bar .droppable > a').click(function(){
	$('.droppable > a').not(this).closest('.droppable').removeClass('active');
	$('.droppable > a').not(this).siblings('.form').hide();
	$(this).closest('.droppable').toggleClass('active');
	$(this).siblings('.form').toggle();
	return false;
});

$('.status-bar .droppable .form').clickOut(function(){
	$(this).closest('.droppable').removeClass('active');
	$(this).hide();
});

//toggles
$('.form .b_icon_close').click(function(){
	$(this).parents('.form').hide();
	$(this).parents('.droppable').toggleClass('active');
});

/*
$('.sort-panel a').click(function(){
	//$(this).toggleClass('active');
	//return false;
});
*/
/*
$('.sort-panel .sort_by a').click(function(e) {
	$(this).closest('.sort_by').find('a').removeClass('active');
	$(this).addClass('active');
    e.preventDefault();
});

$('.sort-panel .sort_by a').click(function(e) {
	$(this).find('i').toggleClass('b_icon_arrowTop b_icon_arrowBot');
	
    e.preventDefault();
});
*/
/*
$('.sort-panel .display a').click(function(){
	$('.sort-panel .display a').removeClass('active')
	$(this).addClass('active');
	return false;
});
*/

$('.sort-panel .display .display-inline').click(function(){

    $(this).parents().find('.product_list').addClass('inline_list');
    $('.product_list').masonry( 'reload' );
});


$('.sort-panel .display .display-grid').click(function(){
    $(this).parents().find('.product_list').removeClass('inline_list');
    
    $('.product_list .product_price').each(function() {
        var productBlock = $(this).closest('.product').find('.wrap');
        $(this).appendTo(productBlock);
    });
    $('.product_list').masonry( 'reload' );
});


/*
$(".sort_type  .btn_check").click(function(){
	$(".sort_type  .btn_check").removeClass('active');
	$(this).toggleClass('active');
	return false;
});
*/
/*
$('.option_type .btn_check2').click(function(){
	$(this).addClass('active');
	return false;
});

$('.option_type .btn_check2 .b_icon_close2').click(function(event){
	$(this).parents('.btn_check2').removeClass('active');
	  event.stopPropagation();
	  return false;
});

$('.option_type .b_icon_close').click(function(){
	$('.option_type .btn_check2').removeClass('active')
	return false;
});

*/





$('.faq .questions ul li h3 span').click(function(){
	$('.faq .questions ul li h3 span').not(this).closest('li').removeClass('active');
	$('.faq .questions ul li h3 span').not(this).parents().siblings('ul').hide();
	$(this).closest('li').toggleClass('active');
	$(this).parents().siblings('ul').slideToggle();
	return false;
});

//tabs 
var activeDataHref=$('.tabs .tab_list .active a').attr('href');
$('.tabs .tab_list .active').parents('.tabs').find('div[data-tab="'+activeDataHref+'"]').show();
$('.tabs .tab_list li').click(function(){
    $('.tabs .tab_list li').removeClass('active');
    $(this).addClass('active');
    $('.tabs .tabs-box > div').hide();
    var dataHref=$(this).find('a').attr('href');
    $(this).parents('.tabs').find('div[data-tab="'+dataHref+'"]').show();
    return false;
});

//fancybox gallery

var currentImage = 0;
$(".fancybox").fancybox({
	    openEffect	: 'elastic',
        closeEffect	: 'elastic',
        nextEffect 	: 'elastic',
        prevEffect	: 'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	false,
		beforeShow:function(){
             
		}
	});


$('.previews a:first-child img').addClass('active');
$('.previews a').click(function(){
	$('.previews a img').removeClass('active');
    $(this).find('img').addClass('active');
    var index=$(this).index();
    $('.product_gallery .main_pic a').hide();
    $(this).parents().find('.main_pic a:eq('+index+')').show();
    return false;
});

     

$.fn.scroller = function(options) {
	var settings = $.extend( {
		marginTop: 10,
		topPanelHeight: 0,
		considerHeight: false,
		onFixedTop: function() {},
		onFixedBottom: function() {},
		onRelax: function() {}
    }, options);

    return this.each(function() {
		var offsetScrollerLeft = $(this).offset(),
			maxDif = $(this).offsetParent().height() - $(this).height() - settings.topPanelHeight,
			marginTop = settings.topPanelHeight + settings.marginTop,
			$that = $(this);
	
		$(window).scroll(function() {
			var dif = $(window).scrollTop() - offsetScrollerLeft.top;
	
			if(settings.considerHeight && $(window).height() < $that.height() + marginTop) {
				$that.removeClass('fixed').css({
					'position': 'relative',
					'top': 0
				});
				
				settings.onRelax();
				return;
			}
	
			if ($(window).scrollTop() > offsetScrollerLeft.top-marginTop && dif < maxDif-marginTop) {
				if (!$that.hasClass('fixed')) {
					$that.addClass('fixed').css({
						'position': 'fixed',
						'top': marginTop
					});
					
					settings.onFixedTop();
				}
			}
			else if ( $(window).scrollTop() < offsetScrollerLeft.top) {
				$that.removeClass('fixed').css({
					'position': 'relative',
					'top': 0
				});
				
				settings.onRelax();
				
			} 
			else {
				$that.removeClass('fixed').css({
					'position': 'relative',
					'top': maxDif
				});
				
				settings.onFixedBottom();
			} 
	
		});
    });
};


var topPanelHeight=$('.top_panel').height();

$('.scroller').scroller({
	topPanelHeight: topPanelHeight,
	marginTop: 30
});

function charsScroll() {
$('.chars li a ').click(function(){
   $('.chars li  ').removeClass('active');
   $(this).parents('li').addClass('active');
   var text=$(this).text();
   $.scrollTo(".sn-pxg U.pxg-set S B:contains('"+text+"')", 500,{ margin:true, offset:-220});
   return false;
});
}

charsScroll();

$('.scroller_alphabet').each(function(index, element) {
	$(this).scroller({
		marginTop: 30,
		topPanelHeight: 65,
		onRelax: function() {
		    $('.shadow-cover').hide();

		},
		onFixedTop: function() {
           $('.shadow-cover').show();
			


		},
		onFixedBottom: function() {
			$('.shadow-cover').hide();
		}
	});
    
});


$('.calc_opt_group.close .calc_opt_list').hide();

$('.calc_opt_group .calc_opt_title span').click(function(){
	$(this).closest('.calc_opt_group').find('.calc_opt_list').slideToggle();
});

$('.calc_opt_group input').each(function(index) {
    $(this).attr('data-id-check', index+1);
});

$('.calc_opt_group .checkbox input').change(function(){
	var id = $(this).attr('data-id-check'),
		text = $(this).closest('li').text(),
		checked = $(this).is(':checked'),
		itemLi;
		
	if (checked) {
		itemLi = '<li data-id-check="' + id + '">\
					<span class="btn_check2 active">' + text + '<i class="b_icon_close2"></i></span>\
				</li>';
		
		$('.calc_selected_list').append(itemLi);
	} else {
		$('.calc_selected_list li[data-id-check="' + id + '"]').remove();
	}
});

$('.calc_selected_list').on('click', '.b_icon_close2', function(){
	var $itemLi = $(this).closest('li'),
		id = $itemLi.attr('data-id-check'),
		inputCheck = $('.calc_opt_group input[data-id-check="' + id + '"]');
	
	inputCheck.prop('checked',false).closest('label').removeClass('checked');
	$itemLi.remove();
});

$('.calc_settings_list .btn_set').click(function(e){
	var dropForm = $(this).closest('li').find('.drop');
	
	$('.calc_settings_list .drop').not(dropForm).fadeOut();
	$('.calc_settings_list .btn_set').removeClass('active');
	
	dropForm.fadeIn();
	$(this).addClass('active');
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

$('.calc_settings_list .b_icon_close').click(function(){
	$(this).closest('li').find('.btn_set').removeClass('active');
});

$('.calc_settings_list .drop').clickOut(function(){
	$(this).fadeOut();
	$(this).closest('li').find('.btn_set').removeClass('active');
});


$('.rating_interactive').each(function(index) {
    var countStars = $(this).attr('data-rate-star'),
		inputs = '', i;
	
	for (i = 0; i < countStars; i++) {
		inputs += '<input type="radio" name="rating' + (index+1) + '" value="' + (i+1) + '" />';
	}
	$(this).html(inputs);
});

$('.rating_interactive').rating(function(vote, event){
	$.ajax({
		url: "/get_votes.php",
		type: "GET",
		data: {rate: vote}
	});
});


// placeholder
$('input[placeholder], textarea[placeholder]').placeholder();


 
  

});
