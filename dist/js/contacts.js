	function initialize() {
	  var mapOptions1 = {
		center: new google.maps.LatLng(56.865619,35.978014),
		zoom: 16,
		panControl: false,
		zoomControl: false,
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: false,
		overviewMapControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	  
	  var myLatlng1 = new google.maps.LatLng(56.865619,35.978014);
	  
	  var map1 = new google.maps.Map(document.getElementById("map-canvas"),
		  mapOptions1);
		  
	  var marker1 = new google.maps.Marker({
			position: myLatlng1,
			map: map1,
			title:"Стройбаза «Метизы»",
			icon: '/wp-content/themes/metizy/img/bg/placemark.png'
	  });
		  
	  var mapOptions2 = {
		center: new google.maps.LatLng(56.850544,35.930089),
		zoom: 16,
		panControl: false,
		zoomControl: false,
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: false,
		overviewMapControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	  
	  var myLatlng2 = new google.maps.LatLng(56.850544,35.930089);
	  
	  var map2 = new google.maps.Map(document.getElementById("map-canvas-2"),
		  mapOptions2);
		  
	  var marker2 = new google.maps.Marker({
			position: myLatlng2,
			map: map2,
			title:"Магазин «Мужской стиль»",
			icon: '/wp-content/themes/metizy/img/bg/placemark.png'
	  });
		  
	}
	google.maps.event.addDomListener(window, 'load', initialize);
