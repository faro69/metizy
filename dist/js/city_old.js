jQuery(document).ready(function($){
 var cdc = $('#cwpp_dizz_city');
 $('#cwpp_dizz_city').after('<div id="city_suggest" class="select-drop"/>');
 var o = cdc.offset();
 if(location.pathname == '/reg/'){
  $('#city_suggest').css({position:'absolute',top:'32px',left:'2px'});
 }else{
  var shift = 32;
  if($('form').find('input[name=cwpp_dizz_inn]').length){
   shift = 68;
  }
  $('#city_suggest').css({position:'absolute',top:o.top+shift,left:o.left,width:cdc.outerWidth()});
 }
 
 var c_close = function(){
  $('#city_suggest').hide();
 }
 var fillinput = function(e){
  var f = $(this).data('fullname').replace(/(<b>)|(<\/b>)/g,'');
  var id = parseInt($(this).data('id'),10);
  $('#cwpp_dizz_city').val(f);
  $('#cwpp_dizz_cityid').val(id);
 }
 var c_done = function(data){
  if(!data.total){
   c_close();
   return false;
  }
  var html = '';
  var r = data.regions;
  for(i in data.data){
   var c = data.data[i];
   var f = '';
   var fdizz = '';
   if(c.id != 207419 && c.id != 208243){
    f = r[c.rid]+', ';
	fdizz = ' ('+r[c.rid]+")";
   }
   f += c.socr+'. '+c.name
   html += '<li data-id="'+c.id+'" data-fullname="'+f+'">'+c.socr+'. '+c.name+fdizz+"</li>\n";
  }
  $('#city_suggest').html('<ul>'+html+'</ul>').show();
  console.info(data);
 }
 var cargs = {
  datatype: 'json',
  done: c_done,
  empty: c_close,
  url: '/ajax/city/',
  delay: 50,
 };
 $('#city_suggest').clickOut(c_close);
 $('#city_suggest ul li').live('click',fillinput);
 $('input#cwpp_dizz_city').suggest(cargs);
 
});