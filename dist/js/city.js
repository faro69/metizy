jQuery(document).ready(function($){
 var cdc = $('#cwpp_dizz_city');
 cdc.after('<div id="city_suggest" class="select-drop"/>');
 var o = cdc.offset();
 if(location.pathname == '/reg/' || location.pathname == '/shop/basket/'){
  $('#city_suggest').css({position:'absolute',top:'32px',left:'2px'});
 }else{
  var shift = 32;
  if($('form').find('input[name=cwpp_dizz_inn]').length){
   shift = 68;
  }
  $('#city_suggest').css({position:'absolute',top:o.top+shift,left:o.left,width:cdc.outerWidth()});
 }
 
 var c_close = function(){
  $('#city_suggest').hide();
 }
 var fillinput = function(e){
  var f = $(this).data('fullname').replace(/(<b>)|(<\/b>)/g,'');
  var id = parseInt($(this).data('id'),10);
  $('#cwpp_dizz_city').val(f);
  $('#cwpp_dizz_cityid').val(id);
 }
 var c_done = function(data){
  if(!data.total){
   c_close();
   return false;
  }
  var html = '';
  for(i in data.data){
   var c = data.data[i];
   var f = '';
   if(c.fullname != ''){
    f += c.fullname+", ";
   }
   f += c.shortname+'. '+c.name;
   html += '<li data-id="'+c.id+'" data-fullname="'+f.replace(/(<b>)|(<\/b>)/g,'')+'">'+f+"</li>\n";
  }
  $('#city_suggest').html('<ul>'+html+'</ul>').show();
 }
 var cargs = {
  datatype: 'json',
  cache: false,
  done: c_done,
  empty: c_close,
  url: '/ajax/city/',
  delay: 50,
 };
 $('#city_suggest').clickOut(c_close);
 $('#city_suggest ul li').live('click',fillinput);
 $('input#cwpp_dizz_city').suggest(cargs);
 
});