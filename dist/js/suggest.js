// ********************************
// Suggest for jquery
// by Creatorus (WBB)
// ver 1.0.0
// ********************************

// проверка на пустое значение
var empty = function(val){
 if(val === false || typeof val == 'undefined' || val === null || val === ''){
  return true;
 }
 if(typeof val == 'string' && val.replace(/\s/g,"") == ''){
  return true;
 }
 return false;
}
// предыдущий запрос 
var prev = '';

;(function($){
$.fn.suggest = function(o){
 o = $.extend({}, $.fn.suggest.options, o);
 var iam = $(this);		// объект формы
 //var prev = '';			// предыдущий запрос		
 var cache = {};		// кэш
 var ajaxsent = false;	// оправлен ли уже запрос - ждем ли
 var trys = 0;			// количество попыток ожижания
 iam.attr('autocomplete', 'off')

 // пришел ответ обрабатываем
 var answer = function(data){
  if(typeof cache[data.q] == 'undefined'){
   cache[data.q] = data;
  }
  ajaxsent = false;
  o.done.call(iam,data);
 }

 // спрашиваем по введенной строке
 var ask = function(){
  var str = $.trim(iam.val());
  if(str.length < o.min || empty(str)){
   o.empty.call(iam);
   return false;
  }
  if(prev != str){
   prev = str;
   if(typeof cache[str] != 'undefined'){
    answer(cache[str]);
	return false;
   }
   o.start.call(iam,str);
   if(!ajaxsent){
    ajaxsent = true;
	var q = {q: str};
	var data = $.extend({}, o.data, q);
    var data_ret = o.data_ext.call(iam,data);
	if(data_ret){
	 data = data_ret;
	}
	$.ajax({
     type: o.type,
     dataType: o.datatype,
     url: o.url,
     data: data,
     success: answer,
    }); 
   }else{
	if(++trys >= o.maxtry){
	 ajaxsent = false;
	 trys = 0;
	}
    setTimeout(ask, o.delay);
   }
  }
 }
 
 // обработчки события отжатия кнопы
 // отслеживаем токо изменение контента
 var keyup = function(e){
  if(e.which >= 48 || e.which == 8 || e.which == 46){
   setTimeout(ask, o.delay);
  }
 }
 
 // биндим событие
 iam.live('keyup',keyup);
 iam.live('keypress',o.keypress);
 
 return this;
};

$.fn.suggest.options = {
 url: '/',					// url для запросов
 type: 'POST',				// тип передачи данных
 datatype: 'json',			// тип данных
 delay: 200,				// задержка отправки запросов
 min: 2,					// минимальное количество символов
 maxtry: 10,				// максимальное количество ожидающих попыток
 data: {},					// данные для отправки по умолчанию
 start: function(str){},	// вызываемая функция в начале поиска
 data_ext: function(o){},	// вызываемая функция в начале поиска для модификации данных
 done: function(data){},	// вызываемая функция по окончании поиска
 empty: function(){},		// если данные пусты или меньше необходимых
 keypress: function(e){},	// функция нажатия на кнопу
}

})(jQuery);