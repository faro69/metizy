/* jQuery Star Rating Plugin
 * 
 * @Author
 * Copyright Nov 02 2010, Irfan Durmus - http://irfandurmus.com/
 *
 * @Version
 * 0.3b
 *
 * @License
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Visit the plugin page for more information.
 * http://irfandurmus.com/projects/jquery-star-rating-plugin/
 *
 */
(function(e){e.fn.rating=function(t){t=t||function(){};this.each(function(n,r){e(r).data("rating",{callback:t}).bind("init.rating",e.fn.rating.init).bind("set.rating",e.fn.rating.set).bind("hover.rating",e.fn.rating.hover).trigger("init.rating")})};e.extend(e.fn.rating,{init:function(t){var n=e(this),r="",i=null,s=n.children(),o=0,u=s.length;for(;o<u;o++){r=r+'<a class="star" title="'+e(s[o]).val()+'" />';if(e(s[o]).is(":checked")){i=e(s[o]).val()}}s.hide();n.append('<div class="stars">'+r+"</div>").trigger("set.rating",i);e("a",n).bind("click",e.fn.rating.click);n.trigger("hover.rating")},set:function(t,n){var r=e(this),i=e("a",r),s=undefined;if(n){i.removeClass("fullStar");s=i.filter(function(t){if(e(this).attr("title")==n)return e(this);else return false});s.addClass("fullStar").prevAll().addClass("fullStar")}return},hover:function(t){var n=e(this),r=e("a",n);r.bind("mouseenter",function(t){e(this).addClass("tmp_fs").prevAll().addClass("tmp_fs");e(this).nextAll().addClass("tmp_es")});r.bind("mouseleave",function(t){e(this).removeClass("tmp_fs").prevAll().removeClass("tmp_fs");e(this).nextAll().removeClass("tmp_es")})},click:function(t){t.preventDefault();var n=e(t.target),r=n.parent().parent(),i=r.children("input"),s=n.attr("title");matchInput=i.filter(function(t){if(e(this).val()==s)return true;else return false});matchInput.attr("checked",true);r.trigger("set.rating",matchInput.val()).data("rating").callback(s,t)}})})(jQuery)