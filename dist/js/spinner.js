;(function($){
var evr = new $.Event('remove');
var orig = $.fn.remove;
$.fn.remove = function() {
 $(this).trigger(evr);
 return orig.apply(this, arguments);
}

var timer = false;
var tiker_off = function(){
 if(timer){
  clearInterval(timer);
 }
}
$.fn.spinner = function(coord){
 var _this = $(this);
 var o = $.fn.spinner.def;
 var frame = 0;
 if(!timer){
  timer = setInterval(function(){
   if(frame >= o.frames-1){
    frame = 0;
   }
   _this.css({backgroundPosition: '0px -'+(++frame*o.width)+'px'});
  },o.time);
 }
 $(this).on('remove',tiker_off);
 return this;
}
$.fn.spinner.def = {
 frames: 12,
 width: 84,
 time: 85,
}
})(jQuery);