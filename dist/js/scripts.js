jQuery(document).ready(function($){
 
 var tcat = 'Купить $1 в Твери. Низкие цены на $1';
 // парсим строку под замену
 // -----------------------------------------
 var parse_string = function(tmpl,str){
  return tmpl.replace(/\$1/g,str.toLowerCase());
 }

 // проверка возможности подключения карты
 // -----------------------------------------
 var say_about_card = function(data){
  //console.info(data);
  o = $('form[name=cwpp_profile] input[name=card]');
  if(data.ok){
   o.addClass('cwpp_fill_ok').removeClass('cwpp_fill_err');
  }else{
   o.addClass('cwpp_fill_err').removeClass('cwpp_fill_ok');
  }
 }
 var check_card = function(){
  var c = $(this).val();
  if(!empty(c)){
   $.ajax({
    cache: false,
    type: 'POST',
    dataType: 'json',
    url: '/ajax/card/',
    data: {
     name: $('form[name=cwpp_profile] input[name=uname]').val(),
     card: c,
    },
    success: say_about_card,
   });
  }else{
   $('form[name=cwpp_profile] input[name=card]').removeClass('cwpp_fill_err');
  }
 }
 $('form[name=cwpp_profile] input[name=card]').live('blur',check_card);

 // регистрация
 // -----------------------------------------
 // расставляем положения тултипсов
 var reset_tooltips = function(){
  $('#form_change_part .tooltip_block.right, .tooltip_block.right.onbasket').each(function(){
   $(this).css('top',-$(this).height()/2+'px');
  });
 }
 // кэш форм для разных типов клиентов
 var cache_reg_form = {};
 var prev_ut = null;
 // показать форму
 var insert_form = function(data){
  var key = $(data).find('#fid').val();
  if(prev_ut != key){
   var o = $('#form_change_part');
   o.slideUp(200,function(){
    o.html(data).slideDown(300,function(){
	 reset_tooltips();
	 check_card.call($('form[name=cwpp_profile] input[name=card]'),null);
	 show_other_input.call($('.select-drop ul#src li.active'),null);
	 show_other_input.call($('.select-drop ul#scope li.active'),null);
	});
   });
   // сохранить кэш
   if(typeof cache_reg_form[key] == 'undefined'){
    cache_reg_form[key] = data;
   }
   prev_ut = key;
  }
 }
 // проверяем и отрабатываем какую
 // форму надо показать - по умолчанию: физ.
 var make_reg_form = function(e){
  var ut = -1;
  var o = $('form[name=cwpp_profile] input[name=usertype]');
  if($(this).hasClass('active') && typeof $(this).data('value') != 'undefined'){
   ut = $(this).data('value');
  }else{
   if(o.val() != ''){
    ut = o.val();
   }
  }
  if(ut < 0){
   ut = 0;
  }
  o.val(ut);
  if(typeof cache_reg_form[ut] != 'undefined'){
   insert_form(cache_reg_form[ut]);
  }else{
   $.ajax({
    cache: false,
    type: 'POST',
    dataType: 'html',
    url: '/ajax/regform/',
    data: {usertype:ut},
    success: insert_form,
   });    
  }
  return false;
 }
 // если выбрали другое - покажем форму
 var show_other_input = function(e){
  var sel = 0;
  var id = $(this).parent('ul').attr('id');
  var h = $('form[name=cwpp_profile] input[name=cwpp_dizz_'+id+']');
  var o = $('form[name=cwpp_profile] .cwpp_dizz_'+id+'_other');
  if(typeof $(this).data('value') != 'undefined'){
   sel = $(this).data('value');
  }
  if(sel){
   h.val(sel);
   if(sel == 99 && o.is(':hidden')){
    o.slideDown(200);
	reset_tooltips();
   }
   if(sel != 99 && o.is(':visible')){
    o.slideUp(200).find('input').val('');
   }
  }
 }
 // отработка событий смены типа
 // и загрузки страницы
 var cwpp = $('form[name=cwpp_profile] input[name=cwpp]');
 if(typeof cwpp != 'undefined' && cwpp.length){
  var cwpp_val = cwpp.val();
  if(cwpp_val == 'reg'){
   make_reg_form.call($('.select-drop ul#usertype li.active'),null);
   $('.select-drop ul#usertype li').live('click',make_reg_form);
  }else{
   if(cwpp_val == 'profile_update'){
    show_other_input.call($('.select-drop ul#src li.active'),null);
    show_other_input.call($('.select-drop ul#scope li.active'),null);
   }
  }
  $('.select-drop ul#scope li, .select-drop ul#src li').live('click',show_other_input);
 }

 // редактирование профиля
 // -----------------------------------------
 var discount = $('.account .discount').text();
 var src_text = $('div.select.src .text-holder').text();
 var scope_text = $('div.select.scope .text-holder').text();
 var src_id = $('.select-drop ul#src li.active').data('value');
 var scope_id = $('.select-drop ul#scope li.active').data('value');
 var src_other_text = $('input[name=cwpp_dizz_src_other]').val();
 var scope_other_text = $('input[name=cwpp_dizz_scope_other]').val();
 $('#change-profile').on('click',function(e){
  var txt = 'Сохранить изменения';
  if($(this).text() != txt){
   $('#buttons_here').append(' <button id="nochange-profile" class="btn pull-r">Отмена</button> ');
   e.preventDefault();
   $(this).text(txt);
   $('input[name=pass]').val('');
   $('.account .discount').text('?');
   $('.account .disabled').removeClass('disabled').removeAttr('readonly');
   return false;
  }
 });
 $('#nochange-profile').live('click',function(e){
  $(this).remove();
  $('#change-profile').text('редактировать данные');
  $('.account .discount').text(discount);
  $('input[name=pass]').val('************');
  $('div.select.src .text-holder').text(src_text);
  $('div.select.scope .text-holder').text(scope_text);
  $('input[name=cwpp_dizz_src_other]').val(src_other_text);
  $('input[name=cwpp_dizz_scope_other]').val(scope_other_text);
  $('.select-drop ul#src li').removeClass('active').each(function(){
   if($(this).data('value') == src_id){
    $(this).addClass('active');
   }
  });
  $('.select-drop ul#scope li').removeClass('active').each(function(){
   if($(this).data('value') == scope_id){
    $(this).addClass('active');
   }
  });
  show_other_input.call($('.select-drop ul#src li.active'),null);
  show_other_input.call($('.select-drop ul#scope li.active'),null);
  $('.account dd .select, .account dd input').addClass('disabled').attr('readonly','readonly');
 });
 
 // Саггест при поиске товаров
 // -----------------------------------------
 // закрываем вкладку
 var drop_close = function(){
  $('.search-drop').hide();
  $('.search-panel .loader').hide();
 }
 $('.search-box').addClass('theone');
 $('.search-box').clone().removeClass('theone').appendTo('.top_panel .container');
 $('.search-panel input[name="q"], .search-panel .search-drop').clickOut(drop_close);
 // долбаная верстка с долбаным клоном
 $('input[name="q"]:focus').live("keyup",function(){
  $('input[name="q"]:not(:focus)').val($(this).val());
 });
 
 // есть результаты поиска
 var s_done = function(data){
  $('.search-panel .loader').hide();
  if(!empty(data)){
   $(".search-drop .drop").html(data);
   $('.search-drop').show();
  }else{
   drop_close();
  }
 }
 // событие ввода
 var s_kp = function(e){
  if(empty($(this).val())){
   drop_close();
  }
 }
 // начало отправки запроса
 var s_start = function(str){
  $('.search-panel .loader').show();
 }
 var search_ext = function(data){
  data.cid = $('input[name=cid]:checked').val();
  return data;
 }
 var args = {
  datatype: 'html',
  done: s_done,
  empty: drop_close,
  keypress: s_kp,
  start: s_start,
  data_ext: search_ext,
  url: '/ajax/search/',
  delay: 50,
 };
 // цеплаемся к саггесту
 $('div.theone input[name="q"]').suggest(args);
 $('div:not(.theone) input[name="q"]').suggest(args);
 
 // Отработка формы ввода количества
 // -----------------------------------------
 // не даем вводить "не цифры" в числовое поле
 $("input.numeric").live('keypress',function(event){
  var controlKeys = [8, 9, 13, 35, 36, 37, 39, 46];
  var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
  if(!event.which || (48 <= event.which && event.which <= 57) || isControlKey){
   if(event.which == 13){
    $(this).trigger("datachange");
   }
   return;
  }else{
   event.preventDefault();
  }
 });
 // устанавливаем значение в форму
 var check_mm_val = function(i,val){
  var min = i.data('min');
  var max = i.data('max');
  if(typeof(min) != 'undefined' && val < min){
   val = min;
  }
  if(typeof(max) != 'undefined' && val > max){
   val = max;
  }
  return val;
 }
 // вышли из поля ввода - проверим на пределы
 $("input.numeric").live('blur',function(event){
  $(this).val(check_mm_val($(this),$(this).val()));
  $(this).trigger("datachange");
 });
 // кнопы "+" и "-"
 $('div.plus,div.minus').live('click',function(event){
  var i = $(this).closest('div.count').find('input.numeric');
  if(typeof i != 'undefined' && typeof i.attr('disabled') == 'undefined'){
   var val = parseInt(i.val(),10);
   if($(this).hasClass('minus')){
    val--;
   }
   if($(this).hasClass('plus')){
    val++;
   }
   i.val(check_mm_val(i,val));
   i.trigger("datachange");
  }
 });
 
 // Добавление в корзину
 // -----------------------------------------
 var goto_basket = function(e){
  e.preventDefault();
  location.href = "/shop/basket/";
  return false;
 }
 var update_basket_widget = function(data){
  var i = $('.b_icon_basket2 .num, .b_icon_basket .num');
  i.animate({opacity: 0}, 200, 'easeOutQuad', function(){
   i.text(data.basket);
   if(data.basket){
    i.animate({opacity: 1}, 200, 'easeOutQuad');
   }
  });
 }
 var basket_ajax = function(id,num,f){
  $.ajax({
   cache: false,
   type: 'POST',
   dataType: 'json',
   url: '/ajax/add/',
   data: {id:id,num:num},
   success: f,
  });
 }
 var add_me = function(e){
  e.preventDefault();
  var id = $(this).attr('href').replace(/#/,'');
  var i = $(this).parents('.price_cart, .compare_product_lead').find('input.numeric');
  var num = i.val();
  basket_ajax(id,num,update_basket_widget);
  i.attr('disabled','disabled').fadeTo(400,0.3);
  $(this).addClass('active').children('span').text("В КОРЗИНЕ");
  return false;
 }
 $('a.add_to_basket:not(.active)').live('click',add_me);
 $('a.add_to_basket.active').live('click',goto_basket);
 
 // пересчитать содержимое корзины
 // -----------------------------------------
 var reset_basket = function(data){
  console.info(data);
  if($('table.product_table').data('usertype')){
   $('#cart_teaser').html(data.teaser);
  }
  $('div.price-area p.price strong').html(data.total_dizz);
  if(data.discount_total_value == 0){
   $('div#dtd').html('');
   $('.discount_abbr').html('');
   if($('table.product_table').data('usertype')){
    nds = (data.total - (data.total/1.18).toFixed(2)).toFixed(2);
    $('div#dtd').html('<p class="discount" style="margin-bottom:-6px;">В том числе НДС: '+nds+' <span class="robule">&#8399;</span></p>');
   }
  }else{
   $('div#dtd').html(data.discount_total_dizz);
   $('.discount_abbr').html(data.abbr_dizz);
  }
  for(var i in data.goods){
   $('span#i'+i).html(data.goods[i]);
   $('span#p'+i).html(data.price[i]);
  }
  reset_tooltips();
 }
 var recount_basket = function(){
  $.ajax({
   cache: false,
   type: 'GET',
   dataType: 'json',
   url: '/ajax/basket/',
   success: reset_basket,
  });
 }
 
 // изменение количества товара в корзине
 // -----------------------------------------
 $('table.product_table input.numeric').live('datachange',function(e){
  basket_ajax($(this).parents('tr').data('gid'),$(this).val(),recount_basket);
 })
 
 // удаление из корзины
 // -----------------------------------------
 var remove_me = function(e){
  e.preventDefault();
  var row = $(this).parents('td').parent('tr');
  var gid = row.data('gid');
  if(row.parents('table.product_table').find('tr').length <= 2){
   $('#basket_content').fadeOut(400,function(){
    $(this).html('<p>В вашей корзине пока нет товаров.</p><br><p>Чтобы начать совершать покупки, перейдите в раздел <a href="/shop/">Магазин</a>,выберите категорию, вид продукции, необходимый товар, затем укажите необходимое количество и нажмите кнопку «В корзину».</p><br><p>Получить более подробную информацию вы сможете, посетив раздел <a href="/info/site/">Помощь по работе с сайтом</a>.</p>').fadeIn(300);
   });
  }else{
   row.animate({opacity:0},200,function(){
    row.find('td').css({padding:0}).height(row.height()).text('').animate({height: 0}, 1000, 'easeOutBounce', function() {
     row.remove();
	});
   });
  }
  basket_ajax(gid,0,function(data){update_basket_widget(data);recount_basket();});
 }
 $('table.product_table td i.b_icon_close4').live('click',remove_me);
 
 // Работаем с историей в html5
 // -----------------------------------------
 var print_filter = function(data){
  var o = $('#filters');
  var v = o.is(":visible");
  if(v){
   o.slideUp(200,function(){
    if(!empty(data)){
     o.html(data).slideDown(200);
    }
   });
  }else{
   if(!empty(data)){
    o.html(data).slideDown(200);
   }
  }
 }
 var get_filter = function(){
  data = {};
  data.cid = parseInt($('section.sort_type a.btn_check.active').attr('id').replace('cat',''),10);
  if(data.cid){
   $.ajax({
    cache: false,
    type: 'GET',
    dataType: 'html',
    url: '/ajax/getfilter/',
    data: data,
    success: print_filter,
   });
  }
 }
 var set_refilter = function(data){
  if($('section.option_type section').length){
   if(data === true){
    $('section.option_type a.btn_check2.noclick').removeClass('noclick');
   }else{
    $('section.option_type section').each(function(i,v){
     var ii = $(this).index();
     if(typeof data[ii] == 'boolean' && data[ii]){
	  $(this).find('a.btn_check2.noclick').removeClass('noclick');
	 }else{
      $(this).find('a.btn_check2').each(function(){
	   var txt = $(this).attr('href').replace(/^#/,'');
	   if($.inArray(txt, data[ii]) > -1){
	    $(this).removeClass('noclick');
	   }else{
	    $(this).addClass('noclick');
	   }
	  });
	 }
    });
   }
  }
 }
 var get_refilter = function(id){
  data = {cid:id};
  if(data.cid){
   $.ajax({
    cache: false,
    type: 'GET',
    dataType: 'json',
    url: '/ajax/getrefilter/',
    data: data,
    success: set_refilter,
   });
  }
 }

 
 var agl_answer = function(data){
  $('#overall_goods_list').html(data);
  $('#ajax-products').masonry('reload');
 }
 var search_answer = function(data){
  $('#overall_goods_list').html($(data).find('#overall_goods_list').html());
  $('#ajax-products').masonry('reload');
 }
 var get_search = function(){
  var q = $('form div.search-panel input[name=q]').val();
  $.ajax({
   cache: false,
   type: 'GET',
   dataType: 'html',
   url: '/search/',
   data: {q:q},
   success: search_answer,
  }); 
 }
 var get_agl = function(id){
  get_refilter(id);
  $('input[name=cid]').val(id);
  $.ajax({
   cache: false,
   type: 'GET',
   dataType: 'html',
   url: '/ajax/goods/',
   data: {cid:id},
   success: agl_answer,
  }); 
 }
 // поддерживаем ли историю
 var h5h = function(){
  return !!(history.pushState && history.state !== undefined);
 }
 // отработка нажатия на кнопу
 $('section.sort_type a.btn_check').on('click',function(e){
  if($(this).hasClass('active')){
   e.preventDefault();
   return false;
  }
  $('section.sort_type a.btn_check').removeClass('active');
  $(this).toggleClass('active');
  var title = $(this).text();
  var href = $(this).attr('href');
  var id = $(this).attr('id').replace(/cat/,'');
  $('input[name=cid]').val(id);
  if($('#baton_category').length){
   $('#baton_category a').text(title).attr('href',href);
  }else{
   $('#breadcrumbs ul li.active').removeClass('active');
   $('#breadcrumbs ul').append('<li class="active" id="baton_category"><a href="'+href+'">'+title+'</a></li>');
  }
  if(h5h()){
   e.preventDefault();
   //document.title = title;
   document.title = parse_string(tcat,title);
   history.pushState({title:title,href:href,id:id}, title, href);
   get_agl(id);
   get_filter(id);
   return false;
  }
 });
 // отработка перехода по истории
 $(window).on('popstate',function(e){
  if(h5h() && history.state){
   $('section.sort_type a.btn_check').removeClass('active');
   $('section.sort_type a.btn_check#cat'+history.state.id).addClass('active');
   if($('#baton_category').length){
    $('#baton_category a').text(history.state.title).attr('href',history.state.href);
   }
   //document.title = history.state.title;
   document.title = parse_string(tcat,history.state.title);
   get_agl(history.state.id);
   get_filter(history.state.id);
  }
  if($('#breadcrumbs ul li.active').index() == 1){
   $('section.sort_type a.btn_check').removeClass('active');
  }
 });
 
 // Устанавливаем настройки просмотра
 // -----------------------------------------
 // установили настройки - перегружаем контент
 var so_answer = function(data){
  var id = false;
  var bid = $('body').attr('id');
  if(bid){
   get_search();
   return false;
  }
  
  if(h5h() && history.state){
   id = history.state.id;
  }else{
   if($('section.sort_type a.active').length){
    id = $('section.sort_type a.active').attr('id').replace(/cat/,'');
   }else{
    if($('h1.pull-l').length){
	 id = $('h1.pull-l').data('cid');
	}
   }
  }
  if(id){
   get_agl(id);
  }
 }
 // установка новых настроек по сортировке и отображению
 var set_order = function(data,reload){
  if(reload){
   var func = so_answer;
  }else{
   var func = null;
  }
  $.ajax({
   cache: false,
   type: 'POST',
   dataType: 'html',
   url: '/ajax/setorder/',
   data: data,
   success: func,
  }); 
 }
 // забираем состояние кноп сортировки и отображения
 var get_order = function(){
  var show = [];
  show[0] = 'cell';
  show[1] = 'row';
  var orders = [];
  orders[0] = 'price';
  orders[1] = 'name';
  orders[2] = 'rating';
  var dir = {};
  dir['b_icon_arrowTop'] = 'ASC';
  dir['b_icon_arrowBot'] = 'DESC';
  var o = $('.sort-panel .sort_by a.active');
  var data = {};
  data['orderby'] = orders[o.index()-1];
  data['order'] = dir[o.find('i').attr('class')];
  o = $('.sort-panel .display a.active');
  data['show'] = show[o.index()-1];
  return data;
 }
 // жмяки по кнопам сортировки
 $('.sort-panel .sort_by a').click(function(e) {
  $(this).closest('.sort_by').find('a').removeClass('active');
  $(this).addClass('active');
  $(this).find('i').toggleClass('b_icon_arrowTop b_icon_arrowBot');
  set_order(get_order(),true);
  e.preventDefault();
  return false;
 });
 // жмяки по кнопам типа отображения
 $('.sort-panel .display a').click(function(e){
  $('.sort-panel .display a').removeClass('active')
  $(this).addClass('active');
  set_order(get_order(),false);
  e.preventDefault();
  return false;
 });

 
 // Работаем с фильтрами в категориях
 // -----------------------------------------
 var filter_sent = false;
 var send_filter = function(){
  if(!filter_sent){
  filter_sent = true;
  var filter = {};
  var cid = parseInt($('section.sort_type a.btn_check.active').attr('id').replace('cat',''),10);
  filter[cid] = {};
  var o = $('section.option_type section a.btn_check2.active');
  if(o.length){
   o.each(function(i,v){
    var val = $.trim($(this).text());
    var col = $.trim($(this).parent().parent('section').index());
    if(typeof filter[cid][col] == 'undefined'){
     filter[cid][col] = [];
    }
    filter[cid][col].push(val);
   });
  }else{
   filter[cid] = false;
  }
  $.ajax({
   cache: false,
   type: 'POST',
   dataType: 'html',
   url: '/ajax/setfilter/',
   data: filter,
   success: function(data){filter_sent=false;so_answer(data);},
  });
  }
 }
 $('.option_type .btn_check2.noclick').live('click',function(e){
  e.stopPropagation();
  e.preventDefault();
  return false;
 });
 $('.option_type .btn_check2:not(.noclick)').live('click',function(){
  $(this).addClass('active');
  send_filter();
  return false;
 });
 $('.option_type .btn_check2 .b_icon_close2').live('click',function(e){
  $(this).parents('.btn_check2').removeClass('active');
  send_filter();
  e.stopPropagation();
  return false;
 });
 $('.option_type .b_icon_close').live('click',function(){
  $('.option_type .btn_check2').removeClass('active');
  send_filter();
  return false;
 });
 
 // Открыть запрашиваемую вкладку faq
 // -----------------------------------------
 if(window.location.pathname == '/info/faq/' && window.location.hash){
  act = window.location.hash.replace('#','');
  $('section.questions ul li#faqs_'+act+' h3 span').click();
 }
 
 // Добавление в избранное
 // -----------------------------------------
 $('a#addtofav').on('click',function(e){
  e.preventDefault();
  var bookmarkUrl = window.document.location;
  var bookmarkTitle = window.document.title;
  if($.browser.mozilla){
   window.sidebar.addPanel(bookmarkTitle, bookmarkUrl,"");
  }else if($.browser.msie){
   window.external.AddFavorite( bookmarkUrl, bookmarkTitle);
  }else if($.browser.opera){
   $(this).attr("href",bookmarkUrl);
   $(this).attr("title",bookmarkTitle);
   $(this).attr("rel","sidebar");
   $(this).click();
  }else{
   alert('Нажмите CTRL+D для добавления в избранное в вашем браузере');
  }
  return false; 
 });
 
 // запросы с сайта
 // -----------------------------------------
 var param = {
  url: '/ajax/form/',
  error_fill: function(fname){
   alert('Не все обязательные поля заполнены '+fname);
   $(this).find('input,textarea,button').attr('disabled',false).css({opacity: 1});
   return false;
  },
  done: function(data){
   if(data.error){
    alert(data.msg);
    $(this).find('input,textarea,button').attr('disabled',false).css({opacity: 1});
 	return false;
   }else{
	$(this).fadeOut(250,function(){
     $(this).parent().find('p').html(data.msg);
	});
   }
  },
  start: function(data){
   $(this).find('input,textarea,button').attr('disabled',true).css({opacity: 0.5});
  },
 }
 $('form#feedbackform').sendform(param);
 $('form#callbackform').sendform(param);
 $('form#faq_ask').sendform(param);
 
 
 // написать / активировать работу с онлайн-консультантом
 // -----------------------------------------
 $('a#online_consult_action').on('click',function(e){
  e.preventDefault();
  $(this).parents('.form').find('.b_icon_close').click();
  o=window.open;o('https://siteheart.com/webconsultation/589820?', 'siteheart_sitewindow_589820', 'width=550,height=400,top=30,left=30,resizable=yes'); return false;
 });
 
 
 // очистить содержимое формы доставки
 // -----------------------------------------
 $('#slide_form .b_icon_close').live('click',function(e){
  e.preventDefault();
  $(this).parents('form').find('input[type=text]').val('');
  return false;
 });
 
 // отработка радио кноп в доставке
 // -----------------------------------------
 $('.delivery-method input[name="method"]').live('change',function(e){
  var m = $(this).val();
  if(m == 1){
   //$('.delivery-method .tabs:after').css({background: 'red'});
   $('#slide_form, .delivery-method .pimpa').animate({opacity:0},10).slideUp(200);
   //$('.delivery-method .price-area').animate({'margin-top' : '0px'}, 200);
   $('#slide_form input').attr('required',false);
  }else{
   $('#slide_form, .delivery-method .pimpa').animate({opacity:1},10).slideDown(200);
   //$('.delivery-method .price-area').animate({'margin-top' : '-23px'}, 200);
   $('#slide_form input').attr('required','required');
  }
 })
 
 
 $('a.btn.print_order').live('click',function(e){
  e.preventDefault();
  var f = window.frames["print_frame"];
  f.document.body.innerHTML = $(this).parents('div.order-row').html();
  f.window.focus()
  f.window.print()
  return false;
 });
 
 $('a.btn.print_basket').live('click',function(e){
  e.preventDefault();
  var f = window.frames["print_frame"];
  f.document.body.innerHTML = $(this).parents('.basket_cart').html();
  f.window.focus()
  f.window.print()
  return false;
 });
 
 
 // выбор параметров 3д дома
 var rot_sent = false;
 var send_filter_3d = function(){
  if(!rot_sent){
    rot_sent = true;
    var tid = $('.rot_type .rot_selector .btn_check2.active').attr('id');
    var purums = [];
    
    $('.rot_type .rot_filter.'+tid+'.active').each(function() {
      if ($(this).find('a.btn_check2.active').length) {
        purums.push($(this).find('a.btn_check2.active').attr('href'));
      }
    });
    $.ajax({
     cache: false,
     /*beforeSend: function ( xhr ) {
      console.info(xhr);
     },*/
     type: 'POST',
     dataType: 'json',
     url: "/?rnd="+Math.random(),
     data: ({id:tid,pur:purums,cwp_ajax:'get_img'}),
     success: function(data){ rot_sent=false; update_rotaui(data); }
    });
  }
 }
 function update_rotaui(data) {
  if (data != false) {
    var array = []; 
    for (var i in data) {
      array[i] = [];
      for (var j in data[i]) {
        array[i].push(data[i][j]);
      }
    }
    //console.info(array);
    $('.rotator').rotatui('change_content', array);
  }
 }
 $('.rot_type .rot_selector .btn_check2').live('click',function(e){
  $('.rot_type .rot_selector .btn_check2').removeClass('active');
  $(this).addClass('active');
  var section = $(this).attr('id');
  $('.rot_type .rot_filter').removeClass('active');
  $('.rot_type .rot_filter.'+section).addClass('active');
  return false;
 });
 
 $('.rot_type .rot_filter .btn_check2.noclick').live('click',function(e){
  e.stopPropagation();
  e.preventDefault();
  return false;
 });
 
 $('.rot_type .rot_filter .btn_check2:not(.noclick)').live('click',function(e){
  e.preventDefault();
  $(this).closest('section').find('a.btn_check2').removeClass('active');
  $(this).addClass('active');
  
  if ($(this).closest('section').hasClass('6')) {
    if ($(this).closest('section').hasClass('type1')) { // тип
      var rd = $(this).data();
      $('.rot_type .rot_filter.6.type2').find('a.btn_check2').each(function(){
        if (!(hasProperty($(this).attr('href'), rd))) { $(this).addClass('noclick'); $(this).removeClass('active'); }
        else $(this).removeClass('noclick');
      });
      if(!$('.rot_type .rot_filter.6.type2 a.btn_check2.active').length) {
        $('.rot_type .rot_filter.6.type2 a.btn_check2:not(.noclick)').first().addClass('active');
      }
    }else{                                              // цвет ???
      
    }
  }
  refresh_params();
  send_filter_3d();
  return false;
 });
 
 $('.rot_type .rot_filter .btn_check2.active').live('click',function(e){
  e.stopPropagation();
  e.preventDefault();
  return false;
 });
 
 $('.rot_type .b_icon_close').live('click',function(e){
  $('.rot_type .rot_filter .btn_check2').removeClass('active');
  $('.rot_type .rot_filter .btn_check2.default').addClass('active');
  refresh_params();
  update_rotaui(img);
  e.stopPropagation();
  return false;
 });
 
 function remark_types (type) {
   $('.rot_type .rot_filter.'+type+'.type1 .btn_check2.active').each(function() {
   
    var rd = $(this).data();
    $('.rot_type .rot_filter.'+type+'.type2').find('a.btn_check2').each(function(){
      if (!(hasProperty($(this).attr('href'), rd))) { $(this).addClass('noclick'); $(this).removeClass('active'); }
      else $(this).removeClass('noclick');
    });
   });
   // collect pars
   refresh_params();
 }
 
 function refresh_params () {
   $('.rot_type .rot_selector .btn_check2').each(function(){
     var tid = $(this).attr('id');
     
     $('.itogo .selector.'+tid).find('header').text($(this).text());
     
     var counter = 0;
     $('.rot_type .rot_filter.'+tid).each(function() {
       if ($(this).find('a.btn_check2.active').length) {
          var ctmps = $('.itogo .selector.'+tid).find('.content');
          if (counter == 0) {
            ctmps.html($(this).find('a.btn_check2.active').text());
          }else{
            ctmps.html( ctmps.html() + "<br>" + $(this).find('a.btn_check2.active').text());
          }
          counter = counter + 1;
       }
     });
   });
 }
 
 remark_types('6');
 
 function hasProperty (val, obj) {
  for(var prop in obj) {
        if(obj.hasOwnProperty(prop) && obj[prop] === val) {
            return true;   
        }
    }
    return false;
  } 
 
 
 
 
 
 
 
 
 
 
 
 
 

});