// ********************************
// Ajax sendform
// by Creatorus (WBB)
// ver 1.0.0
// ********************************

;(function($){
$.fn.sendform = function(o){
 o = $.extend({}, $.fn.sendform.options, o);
 var trys = 0;			// количество попыток
 var iam = $(this);		// объект формы
 var ajaxsent = false;	// оправлен ли уже запрос - ждем ли
 var r = $(this).find('[required]');
 var f = iam.find('input,textarea,select');
 
 // пришел ответ обрабатываем
 var answer = function(data){
  ajaxsent = false;
  o.done.call(iam,data);
  return false;
 }

 // обработка и отправка формы
 var submit = function(e){
  if(typeof e != 'undefined'){
   e.preventDefault();
  }
  if(!ajaxsent){
   o.start.call(iam);
   ajaxsent = true;
   var q = {};
   var fillerr = false;
   if(typeof r == 'object'){
    r.each(function(){
	 if(empty($(this).val())){
	  fillerr = $(this).attr('name');
	  return false;
	 }
	});
    if(fillerr){
     o.error_fill.call(iam,fillerr);
     return false;
    }
   }
   if(typeof f == 'object'){
    f.each(function(){
	 q[$(this).attr('name')] = $(this).val();
	});
   }else{
    return false;
   }
   q._form = iam.attr('id');
   var data = $.extend({}, o.data, q);
   var tmp = o.start.call(iam,data);
   if(typeof tmp != 'undefined' && tmp){
    data = tmp;
   }
   $.ajax({
    cache: false,
    type: 'POST',
    dataType: 'json',
    url: o.url,
    data: data,
    success: answer,
   }); 
  }else{
   if(++trys >= o.maxtry){
    ajaxsent = false;
	trys = 0;
   }
   setTimeout(submit, o.delay);
  }
  return false;
 }
 
 // проверка на пустое значение
 var empty = function(val){
  if(val === false || typeof val == 'undefined' || val === null || val === ''){
   return true;
  }
  if(typeof val == 'string' && val.replace(/\s/g,"") == ''){
   return true;
  }
  return false;
 }

 // биндим событие
 iam.live('submit',submit);
 
 return this;
};

$.fn.sendform.options = {
 url: '/',							// url для запросов
 min: 2,							// минимальное количество символов
 maxtry: 10,						// максимальное количество ожидающих попыток
 delay: 300,						// задержка отправки запросов
 data: {},							// данные для отправки по умолчанию
 start: function(data){},			// вызываемая функция в начале поиска
 done: function(data){},			// вызываемая функция по окончании отправки
 error_fill: function(errs){},		// вызываемая функция при ошибке заполнения
}

})(jQuery);