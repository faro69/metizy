$(document).ready( function(){

//click out
'use strict';

    $.fn.clickOut = function (data, fn) {
        $(document).click(
            $.proxy(function (event) {
                if (!$(event.target).is(this) && this.has(event.target).length === 0) {
                    event.delegateTarget = this.get();

                    if (fn === undefined) {
                        data.call(this, event);
                    } else {
                        fn.call(this, event);
                    }
                }
            }, this)
        );
    };

//sliders
$('#index_slides').slides({
	preload: true,
	preloadImage: '/wp-content/themes/metizy/img/loading.gif',
	play: 6000,
	pause: 1000,
	pagination: true,
	effect:'fade',
	slideEasing: "easeOutBounce",
	slideSpeed:800,
	hoverPause: true
});


$('.step_slider').slides({
    preload: true,
    preloadImage: '/wp-content/themes/metizy/img/loading.gif',
 
    pause: 1000,
    pagination: true,
    slideSpeed:500,
    hoverPause: true
});

slideItemWidth();
function slideItemWidth(){
	var DocumentWidth=$(document).width();
	$('#index_slides .slides_control > div').width(DocumentWidth);
}


$(window).resize(function(){
	slideItemWidth();
});




$('.char-row .words a').live('click',function(){
return false;
});

//tooltips
$('.b_icon_tooltip, .char-row .words a').live('click',function(){
$('.b_icon_tooltip').removeClass('active');
$(this).addClass('active');
$('.tooltip_block').hide();
$(this).find('.tooltip_block').show();
$('.b_icon_tooltip, .char-row .words a').clickOut(function(){
$('.b_icon_tooltip, .char-row .words a').removeClass('active');
$('.tooltip_block').hide();
});

$('.tooltip_block .b_icon_close').live('click',function(){
$('.tooltip_block').hide();
$('.b_icon_tooltip, .char-row .words a').removeClass('active');
return false;
});

});



//tooltips height
var tooltipHeight=$('.tooltip_block.right').height();
$('.tooltip_block.right').each(function(){
	var tooltipHeight=$(this).height();
	$(this).css('top',-tooltipHeight/2+'px');
});

var tooltipHeight2=$('.tooltip_block.left').height();
$('.tooltip_block.left').each(function(){
    var tooltipHeight2=$(this).height();
    $(this).css('top',-tooltipHeight2/2+'px');
});

//Fixed top panel

var notPanels=$('body').not('#index, #er404');
notPanels.prepend('<div class="top_panel"><div class="container"></div></div>');
$('.brand').clone().appendTo('.top_panel .container');
$('.product_bars').clone().appendTo('.top_panel .container');
//$('.search-box').clone().appendTo('.top_panel .container');
$('.top_panel').append('<div class="animate_shadow"></div>');
$('.top_panel').after('<div class="shadow-cover"></div>');
function FixedTop() {
	if (($(window).scrollTop() > 104)) {
		$('.top_panel .animate_shadow').fadeIn(320);
		$('.top_panel').show();
		notPanels.find('header .panel').css('visibility','hidden');

	} else {
		
		$('.top_panel').hide().find('.animate_shadow').hide();
		notPanels.find('header .panel').css('visibility','visible');


	}
}



$(window).scroll(function(){

	FixedTop();
	
	
});
/*
// search drop 
$('.search-panel input[type="text"]').keyup(
	function(){
	   $('.search-panel .loader').show();
	   $('.search-drop').show();
	   $('.search-panel input[type="text"]').val($(this).val());
});



$('.search-panel input[type="text"], .search-panel .search-drop').clickOut(
	function(){
	    $('.search-drop').hide();
	    $('.search-panel .loader').hide();
});
*/

//radio & checkbox 
$('.radio-group .radio label').live('click',function(){
    $(this).parents('.radio-group').find('.radio label').removeClass('checked');
});

$(document).on('click','.checkbox label, .radio label',function(){
    if ($(this).find('input[type="checkbox"], input[type="radio"]').is(':checked')) { 
        $(this).addClass('checked'); } 

    else { 
        $(this).removeClass('checked'); } 
});


//select 

$('.select-holder').live('click',function(){ 
    $('.select-holder').not(this).removeClass('active');
    $('.select-holder').not(this).find('.select-drop').hide();
    
    $(this).toggleClass('active');
    $(this).find('.select-drop').toggle();
    $(this).clickOut(function(){
        $(this).removeClass('active');
        $(this).find('.select-drop').hide();
 });
});

 $('.select-drop li').live('click',function(event){
       $('.select-drop li').removeClass('active');
        $(this).addClass('active');
        $(this).parents('.select-drop').hide();
        $(this).parents('.select-holder').removeClass('active').find('.text-holder').text($(this).text()).addClass('selected');
        event.stopPropagation();
    });

    
 //order history
 $('.order-row .panel').click(function(){
    $(this).toggleClass('current').nextAll('.composition').slideToggle(300);
 });


// masonry products
$('#ajax-products').masonry({
	itemSelector: '.product'
});
$('#ajax-products').masonry( 'reload' );

    $(".char").pxgradient({ 
       step: 2,
       colors: ["#505050","#000"], 
       dir: "y" 
});



// scroll	
$('.faq .questions_list a, .site_help .questions li a').click(function() {
    var str = $(this).attr('href');
    $.scrollTo(str, 500, { margin:false, offset:-75});
    return false;
});




// scroll top
if($(document).find('.scroll_top').length){ 
  var scrollup = function(e){
     var wt = $(window).scrollTop();
     var st = $('.scroll_top');
     if(wt >= 10){
        if(!st.is(':visible') && !st.is(':animated')){
           st.fadeIn(300);
       }
   }else{
    if(st.is(':visible') && !st.is(':animated')){
       st.fadeOut(300);
   }
}}

$(window).bind('scroll',scrollup);
$('.scroll_top').bind('click',function(e){
 e.preventDefault();
 $('html:not(:animated),body:not(:animated)').stop().animate({scrollTop: 0}, 600);

});
}

$('.label').click(function(){
    $(this).toggleClass('checked');
});

/*
var discount=$('.account .discount').text()
$('#change-profile').toggle(
function(){
	$(this).text('Сохранить изменения');
	;
	$('.account .discount').text('?');
	$('.account .disabled').removeClass('disabled').removeAttr('readonly');

	
},
function(){
	$(this).text('редактировать данные');
	$('.account .discount').text(discount);
	$('.account dd .select, .account dd input').addClass('disabled').attr('readonly','readonly');

});
*/

//drops 
$('.status-bar .droppable > a').click(function(){
	$('.droppable > a').not(this).closest('.droppable').removeClass('active');
	$('.droppable > a').not(this).siblings('.form').hide();
	$(this).closest('.droppable').toggleClass('active');
	$(this).siblings('.form').toggle();
	return false;
});

$('.status-bar .droppable .form').clickOut(function(){
	$(this).closest('.droppable').removeClass('active');
	$(this).hide();
});

//toggles
$(document).on('click', '.form .b_icon_close, .form .close_form', function(e){
	e.preventDefault();
  $(this).parents('.form').hide();
	$(this).parents('.droppable').toggleClass('active');
});

/*
$('.sort-panel a').click(function(){
	//$(this).toggleClass('active');
	//return false;
});
*/
/*
$('.sort-panel .sort_by a').click(function(e) {
	$(this).closest('.sort_by').find('a').removeClass('active');
	$(this).addClass('active');
    e.preventDefault();
});

$('.sort-panel .sort_by a').click(function(e) {
	$(this).find('i').toggleClass('b_icon_arrowTop b_icon_arrowBot');
	
    e.preventDefault();
});
*/
/*
$('.sort-panel .display a').click(function(){
	$('.sort-panel .display a').removeClass('active')
	$(this).addClass('active');
	return false;
});
*/

$('.sort-panel .display .display-inline').click(function(){

    $(this).parents().find('.product_list').addClass('inline_list');
    $('.product_list').masonry( 'reload' );
});


$('.sort-panel .display .display-grid').click(function(){
    $(this).parents().find('.product_list').removeClass('inline_list');
    
    $('.product_list .product_price').each(function() {
        var productBlock = $(this).closest('.product').find('.wrap');
        $(this).appendTo(productBlock);
    });
    $('.product_list').masonry( 'reload' );
});


/*
$(".sort_type  .btn_check").click(function(){
	$(".sort_type  .btn_check").removeClass('active');
	$(this).toggleClass('active');
	return false;
});
*/
/*
$('.option_type .btn_check2').click(function(){
	$(this).addClass('active');
	return false;
});

$('.option_type .btn_check2 .b_icon_close2').click(function(event){
	$(this).parents('.btn_check2').removeClass('active');
	  event.stopPropagation();
	  return false;
});

$('.option_type .b_icon_close').click(function(){
	$('.option_type .btn_check2').removeClass('active')
	return false;
});

*/





$('.faq .questions ul li h3 span').click(function(){
	$('.faq .questions ul li h3 span').not(this).closest('li').removeClass('active');
	$('.faq .questions ul li h3 span').not(this).parents().siblings('ul').hide();
	$(this).closest('li').toggleClass('active');
	$(this).parents().siblings('ul').slideToggle();
	return false;
});

//tabs 
var activeDataHref=$('.tabs .tab_list .active a').attr('href');
$('.tabs .tab_list .active').parents('.tabs').find('div[data-tab="'+activeDataHref+'"]').show();
$('.tabs .tab_list li').click(function(){
    $('.tabs .tab_list li').removeClass('active');
    $(this).addClass('active');
    $('.tabs .tabs-box > div').hide();
    var dataHref=$(this).find('a').attr('href');
    $(this).parents('.tabs').find('div[data-tab="'+dataHref+'"]').show();
    return false;
});

//fancybox gallery

var currentImage = 0;
$(".fancybox").fancybox({
		padding: 0,
	    openEffect	: 'elastic',
        closeEffect	: 'elastic',
        nextEffect 	: 'elastic',
        prevEffect	: 'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	false,
		beforeShow:function(){
             
		}
	});


$('.previews a:first-child img').addClass('active');
$('.previews a').click(function(){
	$('.previews a img').removeClass('active');
    $(this).find('img').addClass('active');
    var index=$(this).index();
    $('.product_gallery .main_pic a').hide();
    $(this).parents().find('.main_pic a:eq('+index+')').show();
    return false;
});

     

$.fn.scroller = function(options) {
	var settings = $.extend( {
		marginTop: 10,
		topPanelHeight: 0,
		considerHeight: false,
		onFixedTop: function() {},
		onFixedBottom: function() {},
		onRelax: function() {}
    }, options);

    return this.each(function() {
		var offsetScrollerLeft = $(this).offset(),
			maxDif = $(this).offsetParent().height() - $(this).height() - settings.topPanelHeight,
			marginTop = settings.topPanelHeight + settings.marginTop,
			$that = $(this);
	
		$(window).scroll(function() {
			var dif = $(window).scrollTop() - offsetScrollerLeft.top;
	
			if(settings.considerHeight && $(window).height() < $that.height() + marginTop) {
				$that.removeClass('fixed').css({
					'position': 'relative',
					'top': 0
				});
				
				settings.onRelax();
				return;
			}
	
			if ($(window).scrollTop() > offsetScrollerLeft.top-marginTop && dif < maxDif-marginTop) {
				if (!$that.hasClass('fixed')) {
					$that.addClass('fixed').css({
						'position': 'fixed',
						'top': marginTop
					});
					
					settings.onFixedTop();
				}
			}
			else if ( $(window).scrollTop() < offsetScrollerLeft.top) {
				$that.removeClass('fixed').css({
					'position': 'relative',
					'top': 0
				});
				
				settings.onRelax();
				
			} 
			else {
				$that.removeClass('fixed').css({
					'position': 'relative',
					'top': maxDif
				});
				
				settings.onFixedBottom();
			} 
	
		});
    });
};


var topPanelHeight=$('.top_panel').height();

$('.scroller').scroller({
	topPanelHeight: topPanelHeight,
	marginTop: 30
});

function charsScroll() {

var chsON = false;

$('.chars li a ').click(function(){
   var text=$(this).text(),
     inactive = $(this).parents('li').hasClass('char_inactive');
     
   if (!inactive && !chsON) {
    chsON = true;
    
    $('.chars li  ').removeClass('active');
    $(this).parents('li').addClass('active');
    $.scrollTo(".sn-pxg U.pxg-set S B:contains('"+text+"')", 500,{ margin:true, offset:-140, onAfter: function(){
        setTimeout( function() {chsON = false;}, 10);
        //chsON = false;
      }
    });
   }
   return false;
});

$('.chars li a ').each(function(){ // перемечаем активные буквы
  var text=$(this).text(),
    bukva = $(".sn-pxg U.pxg-set S B:contains('"+text+"')").length;
  if (!bukva) $(this).parents('li').addClass('char_inactive');
});

if (jQuery(document).find(".chars").length) {
  //jQuery(window).bind('load resize scroll', offsetFn);
  
  //var fh = 521; // начальный отступ первой буквы
  var hh = 140; // высота шапки
  var bh = 100; // высота буквы
  var bukvy = $(".char-row");
  var texty = $(".char-row .pxg-source").text();
  var topy = [];
  
  bukvy.each(function() {
    topy.push(Math.round($(this).offset().top) - bh);
  });
  
  var offsetFn = function (e) {
    if (!chsON){
      var wt = $(window).scrollTop();
      var cpos = wt+hh; // текущая позиция сверху
      var len = topy.length;
      
      if (cpos >= topy[len-1]) {
        var span = $(".chars-panel .chars li a span:contains('"+texty[len-1]+"')");
        $('.chars li ').removeClass('active');
        span.parents('li').addClass('active');
      }else {
        for(var b in topy) {
          if ( (cpos >= topy[b] && cpos <= topy[parseInt(b)+1]) ) {
            var span = $(".chars-panel .chars li a span:contains('"+texty[b]+"')");
            $('.chars li ').removeClass('active');
            span.parents('li').addClass('active');
          }
        }
      }
    }
  }
  jQuery(window).bind('load resize scroll', offsetFn);
}

}

charsScroll();

$('.scroller_alphabet').each(function(index, element) {
	$(this).scroller({
		marginTop: 30,
		topPanelHeight: 65,
		onRelax: function() {
		    $('.shadow-cover').hide();

		},
		onFixedTop: function() {
           $('.shadow-cover').show();
			


		},
		onFixedBottom: function() {
			$('.shadow-cover').hide();
		}
	});
    
});


$('.calc_opt_group.close .calc_opt_list').hide();

$(document).on('click', '.calc_opt_group .calc_opt_title span', function(e){
	$(this).closest('.calc_opt_group').find('.calc_opt_list').slideToggle();
});

/*$('.calc_opt_group input').each(function(index) {
    $(this).attr('data-id-check', index+1);
});*/

$(document).on('change', '.calc_opt_group .checkbox input', function(e){
  var 
    //id = $(this).attr('data-id-check'),
		text = $(this).closest('li').text(),
		checked = $(this).is(':checked'),
    id = $(this).data('wid'),
		itemLi;
		
	if (checked) {
		itemLi = '<li data-wid="' + id + '"><span class="btn_check2 active">' + text + '<i class="b_icon_close2"></i></span></li>';
		
		$('.calc_selected_list').append(itemLi);
    updateWorkType(id);
	} else {
		$('.calc_selected_list li[data-wid="' + id + '"]').remove();
    updateWorkType(id, true);
	}
});

$(document).on('click', '.b_icon_close2', function(){
  var $itemLi = $(this).closest('li'),
		id = $itemLi.attr('data-wid'),
		inputCheck = $('.calc_opt_group input[data-wid="' + id + '"]');
    inputCheck.closest('label').trigger('click').removeClass('checked');
    
	//inputCheck.prop('checked',false).closest('label').removeClass('checked');
	//$itemLi.remove();
});

$(document).on('click', '.calc_settings_list .btn_set', function(e){
	var dropForm = $(this).closest('li').find('.drop');
	
	$('.calc_settings_list .drop').not(dropForm).fadeOut();
	$('.calc_settings_list .btn_set').removeClass('active');
	
	dropForm.fadeIn();
	$(this).addClass('active');
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

$(document).on('click', '.calc_settings_list .b_icon_close', function(e){
	$(this).closest('li').find('.btn_set').removeClass('active');
});

$(document).on('clickOut', '.calc_settings_list .drop', function(e){
	$(this).fadeOut();
	$(this).closest('li').find('.btn_set').removeClass('active');
});


$('.rating_interactive').each(function(index) {
    var rating = parseInt($(this).data('rating'),10);
    var countStars = $(this).attr('data-rate-star'),
		inputs = '', i;
	
	for (i = 0; i < countStars; i++) {
	 var c = '';
	 if(i+1 == rating){
	  c = ' checked="checked"';
	 }
		inputs += '<input type="radio" name="rating' + (index+1) + '" value="' + (i+1) + '" ' + c + '/>';
	}
	$(this).html(inputs);
});

$('.rating_interactive').rating();

var rating_sent = false;
var voted = function(data){
 alert(data.msg);
 if(typeof data.rating != 'undefined'){
  $('.rating_title').attr('title','Текущая оценка: '+data.rating);
  var i = $('.rating_title').find('div.stars a');
  i.removeClass('fullStar').eq(parseInt(data.rating,10)-1).addClass('fullStar').prevAll().addClass('fullStar');
 }
 rating_sent = false;
}
$('.rating_interactive').on('set.rating',function(e,val){
 if(!rating_sent){
  var data = {value:val,gid:$(this).data('gid')};
  rating_sent = true;
  $.ajax({
   cache: false,
   type: 'POST',
   dataType: 'json',
   url: '/ajax/setrating/',
   data: data,
   success: voted,
  });
 }
});


// calculator
function setCookie(name, val) {
  $.cookie('calc_enable', 1, { expires : 30 });
  if(val) $.cookie(name, JSON.stringify(val), { expires : 30 });
  else $.cookie(name, '', { expires : -30 });
}

if ($('.calc_progress').length) {
  var clk_step = false;
  var cur_step = false;
  
  // step
  $(document).on('click', '.calc_progress li.avail', function(e) {
    e.preventDefault();
    
    var $me = $(this);
    if ($me.hasClass('already')) return false;
    
    
    clk_step = parseInt($me.data('step'), 10);
    cur_step = parseInt($('.calc_progress li.active').data('step'), 10);
    
    $me.addClass('already');

    // validate 
    if (validate(cur_step,clk_step)) {
      var s_data = {'step': clk_step};
      
      $.ajax({
        type: 'POST',
        url: location.href,
        dataType: 'json',
        data: s_data,
        success: function(res) {
          activeStep(clk_step);
          changeStep3(clk_step);
          $me.removeClass('already');
          $('.calc_form').fadeOut(200, function() {
            $('.calc_form').html(res);
            $('.calc_form').fadeIn(200);
            update_basket_fcalc();
          });
        }
      });
    }else $me.removeClass('already');
  });
  
  // to basket
  $(document).on('click', '.sm_to_basket', function(e){
    e.preventDefault();
    
    var $me = $(this);
    if ($me.hasClass('already')) {
      window.open('/shop/basket/', '_blank');
      return false;
    }
            
    clk_step = 5;
    cur_step = parseInt($('.calc_progress li.active').data('step'), 10);
    var wid = parseInt($me.data('wid'),10);
    
    var s_data = {'step': clk_step, 'wid': wid};
    
    $me.addClass('already');
      
    $.ajax({
      type: 'POST',
      url: location.href,
      dataType: 'json',
      data: s_data,
      success: function(res) {
        $me.html('В корзине <i class="b_icon_basket3"></i>');
        update_basket_fcalc();
      }
    });
  });
  
  // analog
  $(document).on('click', '.choose_analog', function(e){
    e.preventDefault();
    
    var $me = $(this);
    if ($me.hasClass('already')) return false;
                
    clk_step = 4;
    cur_step = parseInt($('.calc_progress li.active').data('step'), 10);
    var wid = parseInt($me.data('wid'),10);
    var grid = parseInt($me.data('grid'),10);
    var gid = parseInt($me.data('gid'),10);        
    
    var s_data = {'step': clk_step, 'wid': wid, 'grid':grid, 'gid':gid};
    
    $me.addClass('already');
      
    $.ajax({
      type: 'POST',
      url: location.href,
      dataType: 'json',
      data: s_data,
      success: function(res) {
        activeStep(3);
        changeStep3(4);
        $me.removeClass('already');
        $('.calc_form').fadeOut(200, function() {
          $('.calc_form').html(res);
          $('.calc_form').fadeIn(200);
          update_basket_fcalc();
        });
      }
    });
  });
  
  $(document).on('click', '.submit_analog, .back_analog', function(e){
    e.preventDefault();
    
    var $me = $(this);
    if ($me.hasClass('already')) return false;
    
    clk_step = 3;
    cur_step = parseInt($('.calc_progress li.active').data('step'), 10);
    var wid = ($me.hasClass('submit_analog'))?parseInt($me.data('wid'),10):-23;
    var grid = ($me.hasClass('submit_analog'))?parseInt($me.data('grid'),10):-23;
    var gid = ($me.hasClass('submit_analog'))?parseInt($me.data('gid'),10):-23;
    
    var s_data = {'step': clk_step, 'wid': wid, 'grid':grid, 'gid':gid};
    
    $me.addClass('already');
      
    $.ajax({
      type: 'POST',
      url: location.href,
      dataType: 'json',
      data: s_data,
      success: function(res) {
        activeStep(clk_step);
        changeStep3(clk_step);
        $me.removeClass('already');
        $('.calc_form').fadeOut(200, function() {
          $('.calc_form').html(res);
          $('.calc_form').fadeIn(200);
          update_basket_fcalc();
        });
      }
    });
  });    
  
  $(document).on('click', '.calc_next_step', function(e){
    e.preventDefault();
    cur_step = parseInt($('.calc_progress li.active').data('step'), 10);
    $('.calc_progress li[data-step='+(cur_step+1)+']').trigger('click');
  });
  
  $(document).on('click', '.calc_prev_step', function(e){
    e.preventDefault();
    cur_step = parseInt($('.calc_progress li.active').data('step'), 10);
    $('.calc_progress li[data-step='+(cur_step-1)+']').trigger('click');
  });
  
  // check number
  $(document).on('keypress change keyup paste', 'input.float', function(event){  
    var controlKeys = [8, 9, 13, 39, 37, 46]; // 44 - ,
    var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
    if(!event.which || (48 <= event.which && event.which <= 57) || isControlKey){
     return;
    }else{
     event.preventDefault();
    }
  });
  $(document).on('keypress change keyup paste', 'input.integer', function(event){  
    var controlKeys = [8, 9, 13, 39, 37]; // 44 - , 46 - .
    var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
    if(!event.which || (48 <= event.which && event.which <= 57) || isControlKey){
     return;
    }else{
     event.preventDefault();
    }
  });
  
  // cook me
  $(document).on('keypress change keyup paste', 'input.cookme', function(event){
    // if webkit 
    if($.browser.webkit) { if(event.type=="keypress") { return; } }
    else { if(event.type=="keyup") { return; } }
    
    var me = $(this);
    setTimeout( function() {
      storeParam(me.data('wid'), me.attr('name'), me.val());
    }, 100);
  });
  
  var update_basket_fcalc = function(){
    var i = $('.b_icon_basket2 .num, .b_icon_basket .num');
    var num = 0;
    
    $.ajax({
      type: 'POST',
      url: location.href,
      dataType: 'html',
      data: {'basket_me':1},
      success: function(res) {
        if(i.first().text() != res) {
          i.animate({opacity: 0}, 200, 'easeOutQuad', function(){
            i.text(res);
            if(res>0){
              i.animate({opacity: 1}, 200, 'easeOutQuad');
            }
          });
        }
      }
    });
  }
  
  // cook me store function
  var storeParam = function(wid,pName,pVal) {
    if(!wid || !pName) return;
    
    var pm = ($.cookie('calc_wparams'))?JSON.parse($.cookie('calc_wparams')):{};
    if (pVal) { // add/update
      if(jQuery.isEmptyObject(pm[wid])) pm[wid] = new Object();
      pm[wid][pName] = pVal;
    }else{ // remove
      if(!jQuery.isEmptyObject(pm[wid])) delete pm[wid][pName];
    }
//console.info(pm);
    setCookie('calc_wparams', pm);
  }
  
  // активация иконки с шагом
  var activeStep = function(step) {
    $('.calc_progress li').removeClass('active back avail');
    $('.calc_progress li[data-step='+step+']').addClass('active');
    $('.calc_progress li[data-step='+(step-1)+']').addClass('back');
    $('.calc_progress li[data-step='+(step+1)+']').addClass('avail');
    $('.calc_progress li').filter(function() {
      return $(this).attr("data-step") < step;
    }).addClass('avail');
  }
  
  var changeStep3 = function(step) {
    var i = $('.calc_progress li[data-step=3] .pic img');
    var p = $('.calc_progress li[data-step=3] p');
    
    if (step == 4) {
      i.attr('src', calc_tp+'/img/pics/m_pic11.png');
      p.html('Выбор аналога');
    }else{
      i.attr('src', calc_tp+'/img/pics/m_pic10.png');
      p.html('Формирование сметы');
    }
  } 
  
  // validate current step
  var validate = function(step,clk_step) {
    step = step || cur_step;
    var res = false;
    
    if (clk_step < step) return true;
    
    switch (step) {
      case 3:
        res = true;
      break
      case 2:
        res = validateS2();
      break
      case 1:
        res = validateS1();
      break
      default:
        alert('Что-то пошло не так');
    }
    
    return res;
  }
  
  var validateS1 = function() {
    var res = true;
    if (!$('.calc_selected_list li').length) {
      res = false;
      $('.calc_errors').html('<p class="calc_error">Выберите Вид работ</p>');
    }
    return res;
  }
  
  var validateS2 = function() {
    var res = true;
    $('.calc_errors').html('');
    $('input.required').each(function() {
      if(!$(this).val()) {
        if ($(this).closest('li').find('.calc_errors').length) {
          $(this).closest('li').find('.calc_errors').html('<p class="calc_error">Пожалуйста, заполните все поля</p>');
        }
        res = false;
      }
    });
    return res;
  }
}

function updateWorkType(id, act) {
  act = act || false; // add by default
  if (!id) return;
  $('.calc_errors').html('');

  var w = ($.cookie('calc_works'))?JSON.parse($.cookie('calc_works')):{};
  if(act) { // remove
    if(id in w){ delete w[id]; }
  }else{ // add/update
    if(!(id in w)){ w[id] = new Object(); }
  }
  setCookie('calc_works', w);
//console.info(w);
}

/*
$('.rating_interactive').rating(function(vote, event){
	$.ajax({
		url: "/get_votes.php",
		type: "GET",
		data: {rate: vote}
	});
});
*/

// placeholder
$('input[placeholder], textarea[placeholder]').placeholder();


 
  

});
