$(document).ready( function(){
// masonry products
$('#ajax-products').masonry({
    itemSelector: '.product'
});

// ajax products
var flag1=true;
var flag2=false;

$(window).scroll(function(){
if($('#pager').length){
	var scrolltop=$(window).scrollTop();
    var curPosTop=$('#ajax-products').offset().top +$('#ajax-products').height();
    var windowHeight=$(window).height();

 if ( (scrolltop > (curPosTop-windowHeight)) || flag2){
      if (flag1) {      
			var cid = $('#pager').data('cid');
			var page = $('#pager').attr('href').replace(/\?page=/,'');
            $.ajax({
			    cache: false,
                url: "/ajax/goods/?cid="+cid+'&page='+page,
                dataType: 'html',
                beforeSend: function(){
                     flag1=false;
					 $('#pager').remove();
                     $('.loader-panel').show();
                },
                success: function(data){
                	$('.loader-panel').hide();
                    $('#overall_goods_list').append(data);
                    $('#ajax-products').masonry( 'reload' );
                      
	                  setTimeout(function(){
	                  	flag1=true;
	                  	if (document.height == $(window).scrollTop() + $(window).height()){
	                         $(window).scroll();
	                         flag2=true;
	                  	}
	                  	else{ flag2=false;}
	                  },250);
                },
                error: function(){
                    alert('ajax error');
                }
            });
			
        }
    }
}
});      

});
